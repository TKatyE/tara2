<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>DonorsCure.org - I am a Donor.</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/overrides.css" rel="stylesheet" media="screen">
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<script src="bootstrap/js/css3-mediaqueries.js"></script>	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <div class="container" style="margin-bottom: 100px;">
    <div class="navbar navbar-fixed-top"></div>
    
    <div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
        <a href="index.php"><img src="logo_working.png" class="logo" style="margin-bottom: 80px;"></a>
      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
    </div>
  <div class="row">
  
  	<div class="col-lg-12 col-xs-12">
  	
  	<h3>I am a Donor.  What is Donors Cure?</h3>
  
  	<p><img src="/donorscure/images/thinking.png" class="cartoon">In the United States, 60% of all scientific research is funded through government 
  	agencies such as the National Health Institute (NIH).  However, the pot of money that 
  	these agencies have available is limited, and competition is fierce.  On top of that, 
  	writing an application for funding (called a 'grant') can be time-consuming, with it 
  	taking one or two years just to generate background data and another nine months or more 
  	for a 'yes' or 'no' decision to be made.  <br /><br />And after all this time and effort? <br /> <br />
  	Less than 10% of these grant applications are actually funded.</p>
  
  	
		<p><img src="/donorscure/images/donorscomputer.png" class="cartoon-computer">Scientists depend on grants to support not only their work, but themselves and their families.  
		Once they reach a certain level that is fairly early in their careers, they are responsible 
		for finding and applying for this funding on their own.  With the grant application process 
		being so competitive and funds being so limited, researchers have no choice but to propose 
		ideas that are almost guaranteed to be successful in favor of ideas that might be riskier 
		but more innovative, and even then, there is a constant cloud hanging over of whether 
		they will receive funding or not.  <img src="/images/convo.png" class="cartoon-convo">
		With researchers spending an estimated 40% of their 
		time either planning, preparing or writing grant applications, there is sometimes little 
		time for actual research to get done. </p>

		<p>Donors Cure is trying to tackle this problem by providing researchers an alternate mechanism to 
		fund biomedical research.  As a donor, you can browse through projects from researchers at accredited universities
		 and recognized institutions across the United States and give any amount to the ones that mean something to you 
		 or simply catch your eye.  In return, you'll get to interact and engage with the researchers you support and be
		  directly involved and connected to them as they try to find the cure.</p>

		<h3><a href="faqs_donors.php" style="font-size: 20px; text-align: center;" target="_blank">Questions?  Here's what other donors have been asking.</a></h3>


		</div>
	</div>
	
	<div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">

  		<form style="margin-top: 40px; margin-bottom: 25%;" method="post" action="send_email_form.php">
        <fieldset>
        
        	<h3 style="font-size: 18px; text-align: center;">Add your email, and we'll keep you updated about our launch!</h3>
        	<div class="form-group">
            <input type="text" class="form-control input-large" name="email" id="email" placeholder="Your Email">
          </div>
            
          
          <p style="margin-top: 60px; margin-bottom: 30px;">If you were to fund a biomedical research project on DonorsCure.org, how often would you
          return to find out updates about the project and researcher you supported?</p>      
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios1" value="fewmonths" checked>
          			Once every few months.
          	</label>
          </div>  
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios2" value="everymonth">
          			Once every month.
          	</label>
          </div>
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios3" value="everyweek">
          			Once a week.
          	</label>
          </div>         
                   
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios4" value="notatall">
          			Not at all.
          	</label>
          </div>         
                 
          <p style="margin-top: 60px; margin-bottom: 30px;">What sorts of projects would you consider supporting? Check all that apply.</p>
       
          <div class="checkbox">
          	<label>
          		<input type="checkbox" name="knowresearcher" value="knowresearcher" id="knowresearcher">
          		I know personally or know of the researcher.
          	</label>
          </div>
          
          <div class="checkbox">
          	<label>
          		<input type="checkbox" name="knowuniversity" value="knowuniversity" id="knowuniversity">
          		I have a connection to the university/institution the research is currently at.
          	</label>
          </div>
          
        	<div class="checkbox">
          	<label>
          		<input type="checkbox" name="knowdisease" value="knowdisease" id="knowdisease">
          		I have a personal interest in the disease/condition they are researching.
          	</label>
          </div>
          
          <div class="checkbox">
          	<label>
          		<input type="checkbox" name="atrandom" value="atrandom" id="atrandom">
          		I would pick projects at random if I like something about them.
          	</label>
          </div>
   
          	<button type="submit" class="btn btn-default email-add">Submit</button>
          </div>        
          
          <div class="col-xs-6 col-lg-3 col-sm-3"></div>

        </fieldset>
      </form>
    </div>
  </div>
  </div>
	<div class="navbar navbar-fixed-bottom"></div>



</body>
</html>