<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>DonorsCure.org - I am a Researcher.</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/overrides.css" rel="stylesheet" media="screen">
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<script src="bootstrap/js/css3-mediaqueries.js"></script>	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <div class="container" style="margin-bottom: 100px;">
    <div class="navbar navbar-fixed-top"></div>
    
    <div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
        <a href="index.php"><img src="logo_working.png" class="logo" style="margin-bottom: 80px;"></a>
      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
    </div>
  
  <div class="row">
  
  	<div class="col-lg-12 col-12 col-xs-12">
  	
  	<h3>I am a Researcher.  What is Donors Cure?</h3>

  	<p>In the United States, 60% of all scientific research is funded through government 
  	agencies such as the National Health Institute (NIH).  <img src="/donorscure/images/thinking.png" class="cartoon">As a researcher, you know how limited these funds are
  	and how fierce competition for them has become.  With the entire process of coming up with an idea, 
  	generating background data, writing a 20-plus page grant application and waiting for an answer taking about two years to complete,
  	your valuable time, when you could be finding the cure, is getting stolen away.  When it comes to selecting which
  	ideas you follow up and and submit for funding, you're most likely to choose the more conservative ones that 
  	might have a slightly better chance of seeing some monetary success rather than the ones that could be the next big breakthrough
  	but might be a bit more 'outside-of-the-box'.</p>
  	
  	<p><img src="/donorscure/images/donorscomputer.png" class="cartoon-computer">Donors Cure is trying to tackle this 
  	problem by providing biomedical researchers like you with an alternate mechanism to fund your work.  As a researcher at a 
  	university participating in Donors Cure, you can post a proposal for a project that members of the general public can then 
  	donate as much as they want to.  When your funding reaches its goal, the money raised is transferred to you via your university 
  	so your research can begin.  While your project is posted and after it is funded, you can engage directly with your donors to 
  	let them know the work they supported is going and connect them directly to the cure.</p>

		<h3><a href="faqs_researcher.php" style="font-size: 20px; text-align: center;" target="_blank">Questions?  Here's what other researchers have been asking.</a></h3>
		</div>
	</div>
	
	<div class="row">

      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
      
  		<form style="margin-top: 40px; margin-bottom: 25%;" method="post" action="send_email_form_researcher.php">
        <fieldset>
        		<h3 style="font-size: 18px; text-align: center;">Add your email, and we'll keep you updated about our launch!</h3>
        		<div class="form-group">
            	<input type="text" class="form-control input-large" name="email" id="email" placeholder="Your Email">
        		</div>    
        
          <p style="margin-top: 60px; margin-bottom: 30px;">If you were to post a project on DonorsCure.org, what amount of funding would
          you aim to raise?</p>      
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios1" value="under5000" checked>
          			Less than $5000
          	</label>
          </div>  
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios2" value="lower">
          			$5000-$10,000
          	</label>
          </div>
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios3" value="mid">
          			$10,000-$25,000
          	</label>
          </div>         
                   
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio" id="optionsRadios4" value="high">
          			Over $25,000
          	</label>
          </div>         
                 
          <p style="margin-top: 60px; margin-bottom: 30px;">How often would you update your project page (i.e. respond to comments, post a short update 
          on your research progress, post something about the disease or field you're working on) to engage with your donors?</p>
       
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio_often" id="optionsRadios1" value="fewmonths" checked>
          			Once every few months.
          	</label>
          </div>  
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio_often" id="optionsRadios2" value="everymonth">
          			Once every month.
          	</label>
          </div>
          
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio_often" id="optionsRadios3" value="everyweek">
          			Once a week.
          	</label>
          </div>         
                   
          <div class="radio">
          	<label>
          		<input type="radio" name="optionsRadio_often" id="optionsRadios4" value="notatall">
          			Not at all.
          	</label>
          </div>   
          <button type="submit" class="btn btn-default email-add">Submit</button>
      
        </div>
        <div class="col-xs-6 col-lg-3 col-sm-3"></div>
        
        </fieldset>
      </form>
    </div>
  </div>
  </div>
	<div class="navbar navbar-fixed-bottom"></div>



</body>
</html>