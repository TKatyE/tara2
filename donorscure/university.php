<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>DonorsCure.org - I am a University.</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/overrides.css" rel="stylesheet" media="screen">
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<script src="bootstrap/js/css3-mediaqueries.js"></script>	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <div class="container" style="margin-bottom: 100px;">
    <div class="navbar navbar-fixed-top"></div>
    
    <div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
        <a href="index.php"><img src="logo_working.png" class="logo" style="margin-bottom: 80px;"></a>
      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
    </div>
  
  <div class="row">
  	<div class="col-lg-12 col-xs-12">
  

  	
  	<h3>I am a University/Institution.  What is Donors Cure?</h3>
  
  	<p style="margin-bottom: 20px;">In the United States, 60% of all scientific research is funded through government 
  	agencies such as the National Health Institute (NIH).  <img src="/donorscure/images/thinking.png" class="cartoon">As a university, you know how limited these funds are
  	and how fierce competition for them has become.  With the entire process of coming up with an idea, 
  	generating background data, writing a 20-plus page grant application and waiting for an answer taking about two years to complete,
  	preparing grant proposals is stealing away an estimated 40% of your researchers' time.  With only about 10% of these proposals ever
  	getting funded, there needs to be another way to keep your researchers engaged and scientific progress going.</p>
  	
  	<p style="margin-top: -10px;"><img src="/donorscure/images/donorscomputer.png" class="cartoon-computer">Donors Cure is a nonprofit organization that is trying to help address this problem by 
  	providing biomedical researchers with an alternate mechanism to fund their work.  By participating in Donors Cure, you
  	will give your researchers the option to post a research proposal on DonorsCure.org that members of the general public,
  	who have a personal connection to the researcher or their work (Maybe their mother has cancer.  Or they're an alumnus/a of 
  	your university.  Or they live in your state.), can fund through donations of any amount.  
  	When donations reach the project's goal, the funds are transferred to the
  	researcher via your university, so their research can begin.  In return?  Donors are directly connected and engaged with the researcher
  	 and are part of 'finding the cure'.</p>
  	 
  	<h3 style="font-size: 20px; text-align: center;"><a href="/faqs_uni.php" target="_blank">Questions?  Here's what other universities and institutions have been asking.</a></h3>  
		<br />
		<h3 style="font-size: 24px; text-align: center;">To learn more about Donors Cure, send us your name, university/institution and email, and we'll be in touch.</h3>

		</div>
	</div>
	
	<div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">

  		<form style="margin-top: 40px; margin-bottom: 25%;" method="post" action="send_email_form_uni.php">
        <fieldset>
        
        	
        	<div class="form-group">
            <input type="text" class="form-control input-large" name="name" id="name" placeholder="Your Name">
          </div>
            
          <div class="form-group">
            <input type="text" class="form-control input-large" name="uni" id="uni" placeholder="Your University/Institution">
          </div>
        	
        	<div class="form-group">
            <input type="text" class="form-control input-large" name="email" id="email" placeholder="Your Email">
          </div>

          <button type="submit" class="btn btn-default email-add">Submit</button>

      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      </fieldset>
    </form>	
    </div>

	<div class="navbar navbar-fixed-bottom"></div>



</body>
</html>