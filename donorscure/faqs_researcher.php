<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>DonorsCure.org - FAQ's for Researchers</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/overrides.css" rel="stylesheet" media="screen">
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<script src="bootstrap/js/css3-mediaqueries.js"></script>	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <div class="container" style="margin-bottom: 100px;">
    <div class="navbar navbar-fixed-top"></div>
    
    <div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
        <a href="index.php"><img src="logo_working.png" class="logo" style="margin-bottom: 80px;"></a>
      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
    </div>
  
  <div class="row">
  
  	<div class="col-lg-12 col-12 col-xs-12">
  	
  	<h2>Frequently Asked Questions (for Researchers)</h2>
  	
  	<h5>What makes Donors Cure different from other science crowdfunding platforms?</h5>
  	<p>Donors Cure focuses specifically on biomedical research that is of the quality to be 
  	funded by traditional grant-funding mechanisms.  Unlike these other platforms, 
  	Donors Cure requires universities as a whole to sign up as members of the organization.  
  	In order create an account to post a project, the researcher leading the project must be 
  	currently employed in a research capacity at a participating university or institution.</p>
  	
  	<h5>What sort of research should my proposal cover?</h5>
  	<p>Your project can focus on anything within the biomedical field; however, 
  	it should relate, at least broadly, to the other research you currently work on.  
  	Within those guidelines, it is your choice.  Think of your proposal like a grant - 
  	it should be based upon some past evidence but represent a new direction in your research.</p>
		<p>For example, you might want to use Donors Cure to raise a small amount of funding to generate 
		some preliminary results that will help you write a proposal for a larger, federal or foundation 
		funded grant or a larger Donors Cure award.  Alternatively, you might already have an established 
		project that you want to push forward, and you could use Donors Cure to raise a larger amount of 
		funding to cover your work on it for another year.  As long as your project is scientifically 
		feasible for the amount of funding you request, you are encouraged to submit it.</p>
		
		<h5>How high should my funding goal be?</h5>
		<p>This is up to you.  When crafting your proposal, you are encouraged to think big and 
		outside-of-the-box, but keep in mind that you have a maximum of six months to reach your 
		funding goal.  Projects seeking smaller amounts of funding (say, $1000-$5000) will take 
		less time to reach their goals but might cover a small period of research or scope, while 
		projects needing larger amounts (upwards of $10,000) might require the full six months but 
		be able to tackle a more substantial research goal.</p>
		
		<h5>What do I need to do to post my project?</h5>
		<p>To post a project on Donors Cure, you will need to first create an account and your 
		researcher profile, which will include your academic CV, some basic information about 
		your training and a photo of yourself.  Once your profile is set-up, you will be able to 
		submit your project for review using a step-by-step online form.  Your proposal will need 
		to include a short description of the project, its goals and significance and a delineated 
		budget, which will be shown on your project page, and a 2- to 3-page more detailed description 
		of the work which will be used by Donors Cure to review internally.  Once your project is approved, 
		it will 'go live' on DonorsCure.org.</p>
		
		<h5>How do I explain my research for my donors?</h5>
		<p>Science is tricky and can get very complicated very quickly. When writing about your 
		research, you need to avoid jargon (unless you very explicitly explain it) or extreme detail.  
		You might want to think of a metaphor for what you do that will help your potential donors 
		visualize your research in a scenario they can understand.  To make it easier to read, your 
		proposal will be split into three short sections: 1) a description of your project, 
		2) the goals of your projects and 3) the significance of your project.</p>
		<p>When you submit your proposal, the Donors Cure scientific staff will review what you are 
		looking to do and how you have written it.  If it needs revised, they'll work with you to 
		edit it appropriately before your proposal goes live.</p>
		
		<h5>Do I need a video?  What should I say?</h5>
		<p>No, a video clip is optional; however, it is strongly recommended.  
		The goal of the video is to personalize your research proposal more and make you more 
		familiar to your potential donors.  Videos should be between one to three minutes.  
		This is your chance to draw people in - talk about who are you, what you want to you and 
		why you're doing what you're doing.  If you are stuck with crafting a script, we can help you.</p>
		
		<h5>When will my project be posted?</h5>
		<p>Donors Cure aims to review all project proposals within one working week, although we 
		can usually go through them much quicker.  When we review your proposal, we will get in 
		touch with you over email to let you know the outcome.  If your project has been rejected, 
		we'll send it back to you with suggestions for how to improve it, and you are more than 
		welcome to revise it and resubmit when you're ready.</p>

		<p>In most cases, proposals will go live immediately; however, if there are a high number 
		of proposals currently posted from your university/institution, we may decide to hold off 
		posting your proposal for a short time.</p>
		
		<h5>What happens if my proposal isn't funded?</h5>
		<p>If the six-month time period has elapsed or you decide to close your project, 
		the donations you have received up until that point will be redirected 
		(at the donor's choosing) to either a) a similar project at your university, 
		b) your university itself or c) Donors Cure Foundation.</p>
		
		<h5>Can I have more than one project open at once?</h5>
		<p>No.  Researchers are only permitted to post one project at a time.  
		If your project closes and you do not receive any funds (i.e. you did not meet your 
		funding goal), you can post a new, different project immediately, but that one cannot 
		be posted again.  If your project is funded, you must wait six months from the date you 
		receive your funding before posting another project.</p>
		
		<h5>How much of what is raised do I receive?</h5>
		<p>All of it.  Donors Cure adds a surcharge to each donation at checkout, 
		and donors have the option to add more to donate to Donors Cure.  
		These funds do not come out of what is raised for your project.</p>
		
		<h5>What happens if my project is funded?</h5>
		<p>When donors give to your project, the money goes directly to Donors Cure.  
		If your project is funded, the amount raised will be transferred from Donors Cure 
		to the grants financial administration department of your university that we have 
		established contact with, who will then transfer the money to you via their established protocol.  
		As soon as you have the funds and are ready, you can begin using them for your project.</p>
		<p>Throughout your research, you are expected to maintain semi-frequent contact with your 
		donors and are encouraged to interact as much as your feel comfortable.  
		What you write about is up to you, but the goal of your updates should be to engage 
		your donors and connect them to the work they funded.  We'll provide you with resources 
		and guidance for this contact to help you get started.</p>










  	
  	</div>
  </div>
  
</body>
</html>
