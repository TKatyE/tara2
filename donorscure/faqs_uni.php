<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>DonorsCure.org - FAQs for Universities</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/overrides.css" rel="stylesheet" media="screen">
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<script src="bootstrap/js/css3-mediaqueries.js"></script>	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <div class="container" style="margin-bottom: 100px;">
    <div class="navbar navbar-fixed-top"></div>
    
    <div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
        <a href="index.php"><img src="logo_working.png" class="logo" style="margin-bottom: 80px;"></a>
      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
    </div>
  
  <div class="row">
  
  	<div class="col-lg-12 col-12 col-xs-12">
  	
  	<h2>Frequently Asked Questions (for Universities)</h2>
  	
		<h5>Why should my university/institution participate in Donors Cure?</h5>
		<p>For your researchers to be eligible to post projects on DonorsCure.org, your university/institution 
		must sign on to participate in Donors Cure.  By participating in Donors Cure, you will:
<br /><span style="padding-left: 40px;">- Join an elite network of leading biomedical research institutions across the US.</span>
<br /><span style="padding-left: 40px;">- Give your researchers the opportunity to receive an alternate source of funding for their work.</span>
<br /><span style="padding-left: 40px;">- Increase your exposure to those who might not otherwise have known about your university/institution</span>
and its research goals.</p>
	
		<h5>What does participating in Donors Cure entail?</h5>
		<p>Currently, there is no cost or long-term commitment to participating in Donors Cure.  
		When you do sign on, we'll get in touch with your fiscal accounting office to set up a 
		procedure for transferring funds from Donors Cure to your university/institution.  
		Additionally, we will contact your research development office to establish a 
		point-of-contact for your university/institution with Donors Cure.</p>
		
		<h5>How do our researchers post project proposals?</h5>
		<p>When you sign on to participate in Donors Cure, we'll help your research development 
		office set up a procedure for submitting proposals to Donors Cure.  You may choose to 
		allow all of your researchers to submit proposals, or you may opt to have your researchers 
		first send their proposals to your research development office and only have the best 
		submitted to Donors Cure.  This will be determined by your university/institution.</p>
		
		<p>Regardless, projects will be submitted to DonorsCure.org via an online form that 
		will require a title of the project, a description of the project written for the 
		general public, a more detailed description of their project for internal review, 
		the researcher's academic CV, a budget breakdown and optionally, a short video 
		describing why the research is important.  Additionally, researchers will be required 
		to upload a more detailed description of their project (a slightly longer version of a 
		'Specific Aims' section of a grant) giving further information about the proposed 
		experimental process, which will be used by Donors Cure during the internal proposal review process.</p>
		
		<h5>What sort of projects do you expect to be posted to DonorsCure.org?</h5>
		<p>We anticipate Donors Cure to become a great alternate source of funding for 
		early career researchers (i.e. those who are moving from post-doc to assistant 
		professor levels or new faculty); however, as projects are posted and funded, 
		we expect the scope of projects and amount funding they are seeking to raise 
		to find a natural medium.  In terms of what researchers may post - one researcher 
		might be looking for $10,000 to run 10 MRI scans on patients to generate pilot 
		data to secure a larger federal or foundation grant, another might need $50,000 
		to hire a technician or post-doc to help them for a year, another might request 
		$500 to attend a conference that they want to present their work at... there is 
		no restriction.  As long as the proposal meets our criteria, the researcher may 
		post it to DonorsCure.org.</p>

		<p>Aside from meeting our review criteria, the only other requirement is that 
		each research may only have one proposal at a time open for funding on DonorsCure.org.</p>
		
		<h5>How are projects reviewed?</h5>
		<p>First, only scientists employed at your university/institution in a research capacity 
		are eligible to submit projects to Donors Cure.  When we receive a project proposal, 
		we will first verify that the researcher is who they say they are and that they meet 
		this basic requirement.</p>

		<p>Next, we will review each proposal against a set of criteria to assess that it 
		meets our requirements for what constitutes 'biomedical research', the research 
		project is reasonable given the researcher's background and training, the idea 
		is scientifically sound and the proposed research is feasible for the budget requested.  
		While we want to ensure that the proposals posted to DonorsCure.org are for valid projects, 
		we also do not want to restrict researchers from posting ideas that might be slightly 
		more 'outside-of-the-box' but show potential for innovation.</p>
		
		<h5>How long are projects open for funding, and what if a project isn't fully funded?</h5>
		
		<p>To receive funding, projects must raise the full amount requested.  
		When a donors pledges money to a project, they will be given the option to select 
		how that money will be redistributed if the project does not reach its funding goal by 
		the end of the maximum six-month fundraising period.  These options will include: 
		a) reallocate their donation to a similar project at the same university/institution, b) reallocate their
		donation to a similar project anywhere or c) donate the funds to Donors Cure Foundation directly.</p>
		
		<h5>How do researchers get their funding?</h5>
		<p>When your university signs on to participate in Donors Cure, we will contact your 
		fiscal accounting office (or equivalent) and establish a procedure to transfer the funds 
		raised from Donors Cure to the appropriate researcher.  We will work specifically with 
		your accounting office to determine the most efficient way to accomplish this.</p>
		
		<h5>Is there anything required from researchers in return?</h5>
		<p>A key aspect to Donors Cure is connecting donors directly to the people who are 
		doing cutting edge research. Each project page will have a 'journal' section, where 
		researchers are able to post short updates (written text, videos, images, etc.) 
		on their research both during the research fundraising process and after their project is funded.  
		We strongly encourage researchers to engage with their donors (and potential donors) 
		by updating this page about their progress - good or bad.  Donors will also have the option 
		to reply to posts with comments, and we hope that researchers will take this opportunity 
		to communicate with those supporting them.  Since communicating science to non-scientists 
		can be difficult, Donors Cure will have guidance available to help scientists better 
		explain their research.</p>

		<p>If and when a researcher publishes a paper or poster or is recognized for their research, 
		they will be required to disclose this to Donors Cure and post a link on their project page.  
		Over time, researchers will be able to develop a track record on DonorsCure,org that donors 
		can look at when they browse through projects.</p>
		
		<h5>What if my researchers are afraid of having their ideas stolen?</h5>
		<p>Many researchers are hesitant to post their research ideas in the public domain 
		for fear of it being 'scooped'.  While this is a legitimate concern, the part of 
		the proposals that are posted online at DonorsCure.org are written in terms that 
		the general, non-scientific population can easily understand and should focus more 
		on what the research is for and why it is important, rather than how exactly it is 
		going to be completed in the laboratory.  The more technical details of the project 
		will be used internally within Donors Cure to review the project.  Think of what is 
		posted for donors to read as a simplified abstract or expose in an article versus a 
		complete grant application.</p>
	
		<p>Additionally, by creating a central community of leading researchers across the 
		United States, we hope to support a collaborative, rather than competitive, effort 
		among the scientists using Donors Cure.  As such, we encourage researchers to engage
		 with not only their donors, but also the other researchers both within and outside 
		 of their disciplines to develop new ideas and scientific partnerships.</p>



  	</div>
	</div>

</body>


</html>